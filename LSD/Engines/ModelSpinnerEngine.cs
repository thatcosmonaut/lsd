﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Microsoft.Xna.Framework;

namespace LSD.Engines
{
    [Reads(typeof(TransformComponent), typeof(ModelComponent))]
    [Sends(typeof(SetTransformMessage))]
    public class ModelSpinnerEngine : Engine
    {
        private float time;

        public override void Update(double dt)
        {
            time += (float)dt;

            foreach(var entity in ReadEntities<ModelComponent>())
            {
                var transform = GetComponent<TransformComponent>(entity);

                SetTransformMessage setTransform;
                setTransform.entity = entity;
                setTransform.position = transform.position;
                setTransform.orientation = Quaternion.CreateFromYawPitchRoll(time, time, time);
                SendMessage(setTransform);
            }
        }
    }
}
