using Encompass;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BulletSharp;
using Encompass3D;
using Encompass3D.Engines;
using Encompass3D.Renderers;
using Encompass3D.Components;
using Encompass3D.Messages;
using LSD.Engines;

namespace LSD
{
    class LSDGame : Game
    {
        GraphicsDeviceManager graphics;

        WorldBuilder WorldBuilder { get; } = new WorldBuilder();
        World World { get; set; }

        public LSDGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";

            Window.AllowUserResizing = true;
            IsMouseVisible = true;
        }

        protected override void LoadContent()
        {
            WorldBuilder.AddEngine(new RigidbodyEngine());
            WorldBuilder.AddEngine(new PhysicsEngine());
            WorldBuilder.AddEngine(new TransformEngine());
            WorldBuilder.AddEngine(new ModelEngine());
            WorldBuilder.AddEngine(new CameraEngine());
            WorldBuilder.AddEngine(new ModelSpinnerEngine());

            WorldBuilder.AddGeneralRenderer(new ModelRenderer(), 0);

            //i have to use the content manager here because there's no easy way to just load models into xna. dankwraith is working on a solution i believe
            var testModel = Content.Load<Model>("bin/test");

            Entity testEntity = WorldBuilder.CreateEntity();

            SetTransformMessage setTransform;
            setTransform.entity = testEntity;
            setTransform.position = Vector3.Zero;
            setTransform.orientation = Quaternion.Identity;
            WorldBuilder.SendMessage(setTransform);

            SetModelMessage setModel;
            setModel.entity = testEntity;
            setModel.model = testModel;
            WorldBuilder.SendMessage(setModel);

            var boxShape = new BoxShape(1);

            CreateRigidbodyMessage createRigidbody;
            createRigidbody.constructionInfo = new RigidBodyConstructionInfo(1f, null, boxShape);
            createRigidbody.constructionInfo.LocalInertia = boxShape.CalculateLocalInertia(1f);
            createRigidbody.entity = testEntity;
            createRigidbody.orientation = setTransform.orientation;
            createRigidbody.position = setTransform.position;
            WorldBuilder.SendMessage(createRigidbody);

            Entity testCamera = WorldBuilder.CreateEntity();

            SetTransformMessage setCameraTransform;
            setCameraTransform.entity = testCamera;
            setCameraTransform.orientation = Quaternion.Identity;
            setCameraTransform.position = new Vector3(0f, -5f, 0f);
            WorldBuilder.SendMessage(setCameraTransform);

            SetCameraMessage setCamera;
            setCamera.entity = testCamera;
            setCamera.graphicsDeviceManager = graphics;
            setCamera.farPlane = 200;
            setCamera.nearPlane = 0.1f;
            setCamera.fieldOfView = MathHelper.PiOver4;
            setCamera.verticalTilt = 0f;
            WorldBuilder.SendMessage(setCamera);

            var collisionConf = new DefaultCollisionConfiguration();
            var dispatcher = new CollisionDispatcher(collisionConf);
            var broadphase = new DbvtBroadphase();

            InitializePhysicsMessage initializePhysics;
            initializePhysics.world = new DiscreteDynamicsWorld(dispatcher, broadphase, null, collisionConf);
            WorldBuilder.SendMessage(initializePhysics);

            World = WorldBuilder.Build();
             
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            World.Update(gameTime.ElapsedGameTime.TotalSeconds);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            World.Draw();

            base.Draw(gameTime);
        }
    }
}
